import abc
import logging
import pathlib
import random
import sys
import timeit
from dataclasses import dataclass

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from networkx import bellman_ford_path, dijkstra_path, connected_components, \
    grid_graph, astar_path


@dataclass
class Params:
    seed: int
    n_vertices: int
    n_egdes: int


class DataWrapper:

    def __init__(self, params: Params):
        self.params: Params = params
        self.random = random.Random(self.params.seed)
        self.data = None

    def _create_random_edge(self, data):
        for i in range(1000):  # try 1000 times
            v1 = self.random.randint(0, self.params.n_vertices - 1)
            v2 = self.random.randint(0, self.params.n_vertices - 1)
            if v1 == v2:  # self: ignore
                continue
            if data[v1][v2] != 0:
                continue  # already exists: ignore
            return v1, v2
        raise Exception(f"Couldn't create edge after {i + 1} attempts")

    def prepare_data(self) -> 'DataWrapper':
        """
        Prepares the data for the algorithms
        """
        assert self.data is None

        data = np.zeros([self.params.n_vertices, self.params.n_vertices], dtype=int)
        for _ in range(self.params.n_egdes):
            v1, v2 = self._create_random_edge(data)
            logging.debug("v1, v2 = %s, %s", v1, v2)
            assert data[v1][v2] == 0
            assert data[v2][v1] == 0
            data[v1][v2] = data[v2][v1] = self.random.randint(1, 99)

        assert (data == data.transpose()).all()
        self.data = data
        return self


class Task(abc.ABC):
    def __init__(self, data_wrapper: DataWrapper):
        self.data_wrapper = data_wrapper


class Main:
    def __init__(self, params: Params):
        logging.info("params=%s", params)
        self.params: Params = params
        self.data_wrapper: DataWrapper = None
        self.task = None
        self.adjacency_list = None
        self.graph = None

    def log_connected_components(self):
        assert self.graph is not None
        ccs = list(connected_components(self.graph))
        for cc in ccs:
            print(f"Connected component -> {cc}")
        return ccs

    def log_adjacency_list(self):
        logging.info("---------- Full adjacency list")
        assert self.adjacency_list
        for v1 in range(0, self.data_wrapper.params.n_vertices):
            v2s = sorted(list(self.adjacency_list[v1]))
            row_str = " ".join([str(_) for _ in v2s])
            print(f"[{v1:>3}] -> {row_str}")

    def log_first_matix_rows(self):
        logging.info("---------- Print first 20 rows / 30 cols of matrix")
        row_str = " ".join([str(_ % 10) for _ in range(30)])
        print(f"         {row_str}")
        for row in range(len(self.data_wrapper.data[0])):
            row_str = " ".join([str(int(_)) for _ in self.data_wrapper.data[row][0:30]])
            print(f"[{row:>3}] -> {row_str} (...)")

    def populate_adjacency_list(self):
        logging.info("---------- populate_adjacency_list()")
        assert self.adjacency_list is None
        adjacency_list = [set() for _ in range(self.data_wrapper.params.n_vertices)]
        for row in range(len(self.data_wrapper.data[0])):
            for col in range(row + 1):
                if self.data_wrapper.data[row][col] != 0:
                    adjacency_list[row].add(col)
                    adjacency_list[col].add(row)

        self.adjacency_list = adjacency_list

    def setup(self):
        logging.info("---------- setup()")
        assert self.params is not None
        assert self.data_wrapper is None
        self.data_wrapper = DataWrapper(self.params).prepare_data()
        self.task = Task(self.data_wrapper)
        self.populate_adjacency_list()
        self.create_networkx_graph()
        self.log_connected_components()
        self.log_first_matix_rows()
        self.log_adjacency_list()
        with pathlib.Path('data/matrix.csv').open('w') as output_file:
            for row in range(len(self.data_wrapper.data[0])):
                row_str = " ".join([str(_) for _ in self.data_wrapper.data[row]])
                output_file.write(f"{row_str}\n")

    def part1(self):
        v_start = 3
        v_ends = (28, 60, 4, 12, 35, 61, 97, 28, 49, 70, 83, 24, 82)

        def search_dijkstra_custom_implementation(print_result=False):
            for v_end in v_ends:
                result = list(self.dijkstra(v_start, v_end))
                if print_result:
                    print(f"CUSTOM: dijkstra({v_start}, {v_end}) -> {result}")

        def search_dijkstra(print_result=False):
            for v_end in v_ends:
                result = list(dijkstra_path(main.graph, v_start, v_end))
                if print_result:
                    print(f"dijkstra_path({v_start}, {v_end}) -> {result}")

        def search_bellman_ford(print_result=False):
            for v_end in v_ends:
                result = list(bellman_ford_path(main.graph, v_start, v_end))
                if print_result:
                    print(f"bellman_ford_path({v_start}, {v_end}) -> {result}")

        time_dijkstra_custom = (sum(timeit.repeat(
            search_dijkstra_custom_implementation, number=10)) / 10.0) * 1000.0
        time_dijkstra = (sum(timeit.repeat(
            search_dijkstra, number=10)) / 10.0) * 1000.0
        time_bellman_ford = (sum(timeit.repeat(
            search_bellman_ford, number=10)) / 10.0) * 1000.0

        print(f"search_dijkstra_custom_implementation() x 10 -> "
              f"{time_dijkstra_custom}")
        print(f"search_dijkstra() x 10 -> "
              f"{time_dijkstra}")
        print(f"search_bellman_ford() x 10 -> "
              f"{time_bellman_ford}")

    def _dijkstra_get_unvisited_vertex_smallest_distance(
            self, distance, unvisited_vertices) -> int:
        """
        Returns VERTEX, or None if there is no more vertices to try
        (this happens when graph is not connected)
        """
        assert len(unvisited_vertices) > 0
        vertices_distances = [
            (i, distance[i])
            for i in unvisited_vertices
            if distance[i] is not None
        ]
        if not vertices_distances:
            return None

        sorted_vertices = sorted(vertices_distances, key=lambda item: item[1])
        return sorted_vertices[0][0]

    def dijkstra(self, v_start: int, v_end: int):
        prev = [None for _ in range(self.data_wrapper.params.n_vertices)]
        distance = [None for _ in range(self.data_wrapper.params.n_vertices)]
        visited_vertices = set()
        unvisited_vertices = set(range(self.data_wrapper.params.n_vertices))

        distance[v_start] = 0

        cur_vertex = v_start
        while cur_vertex != v_end:
            cur_vertex = self._dijkstra_get_unvisited_vertex_smallest_distance(
                distance, unvisited_vertices)
            if cur_vertex is None:
                return None
            # logging.info("cur_vertex=%s", cur_vertex)
            next_vertices = self.adjacency_list[cur_vertex]
            for vertex_i in next_vertices:
                new_distance = distance[cur_vertex] + \
                               self.data_wrapper.data[cur_vertex][vertex_i]
                if distance[vertex_i] is None or new_distance < distance[vertex_i]:
                    # new best distance
                    distance[vertex_i] = new_distance
                    prev[vertex_i] = cur_vertex

            visited_vertices.add(cur_vertex)
            unvisited_vertices.remove(cur_vertex)

        assert prev[v_end] is not None or v_start == v_end, \
            f"Vertex {v_start} is not connected to {v_end}"

        path_vertices = []
        path_vertex_i = v_end
        while path_vertex_i is not None:
            path_vertices.insert(0, path_vertex_i)
            path_vertex_i = prev[path_vertex_i]

        return path_vertices

    def create_networkx_graph(self):
        assert self.graph is None
        assert self.adjacency_list is not None
        # populate the graph
        graph = nx.Graph()
        for v1 in range(0, len(self.adjacency_list)):
            graph.add_node(v1)
            for v2 in self.adjacency_list[v1]:
                graph.add_edge(v1, v2, weight=self.data_wrapper.data[v1][v2])

        self.graph = graph

    def create_grid(self):
        fig_data, ax_data = plt.subplots()

        grid = grid_graph(dim=(10, 10))
        all_vertices = set(list(grid.nodes))
        obstacles = set()
        while len(obstacles) != 30:
            obstacles.add(self.data_wrapper.random.choice(list(grid.nodes)))

        if pathlib.Path('data/grid.pdf').exists():
            logging.warning("Won't write plot, file exists: data/grid.pdf")
        else:
            pos = nx.spring_layout(grid, iterations=1000)
            options = {"node_size": 100, "alpha": 0.8}
            nx.draw_networkx_nodes(
                grid, pos, nodelist=list(all_vertices - obstacles),
                node_color="#ddd", **options)
            nx.draw_networkx_nodes(grid, pos, nodelist=list(obstacles),
                                   node_color="black", **options)
            nx.draw_networkx_edges(grid, pos, width=0.8, alpha=0.3)
            nx.draw_networkx_labels(grid, pos, font_size=8, alpha=0.7)
            ax_data.axis("off")
            logging.info("Wrinting plot to data/grid.pdf")
            fig_data.savefig("data/grid.pdf")

        for node in obstacles:
            grid.remove_node(node)

        return grid

    def part2(self):
        grid = self.create_grid()
        v_star = (0, 1)
        v_end = (9, 8)

        def approx_euclidean(xy_1, xy_2):
            (x1, y1) = xy_1
            (x2, y2) = xy_2
            return ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5

        print(f"astar_path({v_star}, {v_end}) -> "
              f"{astar_path(grid, v_star, v_end, heuristic=approx_euclidean)}")

        for _ in range(5):
            v_star = self.data_wrapper.random.choice(list(grid.nodes))
            v_end = self.data_wrapper.random.choice(list(grid.nodes))
            print(f"astar_path({v_star}, {v_end}) -> "
                  f"{astar_path(grid, v_star, v_end, heuristic=approx_euclidean)}")

    def plot(self):
        logging.info("---------- PLOT")
        assert self.data_wrapper
        assert self.adjacency_list
        assert self.graph

        fig_data, ax_data = plt.subplots()
        ax_data.axis("off")

        pos = nx.spring_layout(self.graph, iterations=1000)

        nx.draw_networkx(self.graph,
                         pos,
                         with_labels=True,
                         ax=ax_data,
                         node_size=80,
                         width=0.5,
                         font_size=6,
                         font_color='#eee',
                         alpha=0.7)
        if not pathlib.Path('data/graph.pdf').exists():
            logging.info("Wrinting plot to data/graph.pdf")
            fig_data.savefig("data/graph.pdf")
        else:
            logging.warning("Won't write plot, file exists: data/graph.pdf")


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging(level=logging.INFO)
    main = Main(Params(seed=50619808, n_vertices=100, n_egdes=500))
    main.setup()
    main.part1()
    main.plot()

    main.part2()

    logging.info("Finished OK")
